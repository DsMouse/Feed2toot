#!/bin/sh
MODE="$1"
ITEM="$2"

echo Mode: $MODE 
if [ -n "$MODE" ] && [ "$MODE" == "setup" ] ; then 
echo Beginning Setup
if [ -z "$ITEM" ] ; then 
ITEM="first"
fi
cd /tmp/
register_feed2toot_app 
mkdir /etc/feed2toot/"$ITEM"/
cp feed* /etc/feed2toot/"$ITEM"/
feed2toot --populate-cache -v -c /etc/feed2toot/"$ITEM"/feed2toot.ini

grep "^${ITEM}$" /etc/feed2toot/item_list || echo "$ITEM" | tee -a /etc/feed2toot/item_list

elif [ -n "$MODE" ] && [ "$MODE" == "cache" ] ; then 
echo Catching up
cd /tmp/

if [ -n "$ITEM" ] ; then 
feed2toot --populate-cache -v -c /etc/feed2toot/"$ITEM"/feed2toot.ini
else
for ITEM in $(cat /etc/feed2toot/item_list) ; do 
feed2toot --populate-cache -v -c /etc/feed2toot/"$ITEM"/feed2toot.ini
done
fi


else
if [ -z "$MODE" ] ; then MODE="1m" ; echo using default sleep time of 1 minute ; fi

while sleep $MODE ; do 
if [ -n "$ITEM" ] ; then 
feed2toot  -v -c /etc/feed2toot/"$ITEM"/feed2toot.ini
else
for ITEM in $(cat /etc/feed2toot/item_list) ; do 
feed2toot -v -c /etc/feed2toot/"$ITEM"/feed2toot.ini
done # for item 
fi
if [ "$MODE" == "0" ] ; then break 2 ; fi

done #while sleep

fi
