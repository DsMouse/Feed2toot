# Set the base image
# Use an official Python runtime as a parent image
#FROM docker.io/alpine
FROM docker.io/frolvlad/alpine-python3
# Set the base image
# Dockerfile author / maintainer 

## Set the working directory to /app
#WORKDIR /config
VOLUME [ ("setup", "/setup") ]

# Copy the current directory contents into the container at /app
ADD [ "setup.sh", "setup.sh" ]

RUN /setup.sh

# Define environment variable
#ENV NAME World
#FROM   docker.io/centos
USER feed2toot
ADD [ "start.sh", "start.sh" ]
ENTRYPOINT [ "/start.sh" ]
