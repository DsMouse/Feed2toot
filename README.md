# Feed2toot

https://carlchenet.com/get-your-rss-feeds-to-mastodon-with-the-feed2toot-bot/

This is a Alpine/Python3 build with feed2toot-bot RSS-> Mastodon 

Steps to run:
1. Create a mastodon account for it somewhere
2. Create a directory for it's files. I'll use /mnt/data/feed2toot here. 
3. In that directory, create these files acording to the documentation for feed2toot:
feed2toot.ini           hashtags.txt    uri_list.txt 

For the Feed2toot.init, the only difference should be that all files should be in /etc/feed2toot, including the credential files and cache file.  Here's the credintial lines I have as an example:
user_credentials=/etc/feed2toot/feed2toot_usercred.txt
client_credentials=/etc/feed2toot/feed2toot_clientcred.txt

4. Run the setup.  This will create the encrypted credential files.  
docker run -it --rm --name feed2toot -v /mnt/data/mastodon-feed1:/etc/feed2toot www.dsmouse.net/feed2toot setup first
You can re-run with something other than first if you want to run more than one account out of the same docker container


5. Run it... it takes the arguement of how often it pools, with a default of 1 minute ("1m").  Put "0" to run once and exit.
docker run -d --name feed2toot -v /mnt/data/mastodon-feed1:/etc/feed2toot  www.dsmouse.net/feed2toot 1m


