#!/bin/sh

apk add py-pip3
apk add gcc python3-dev musl-dev libffi-dev
apk add openssl-dev

pip3 install configparser
pip3 install feed2toot
adduser --home /var/lib/feed2toot --gecos "" feed2toot
mkdir -p /etc/feed2toot/ /var/lib/feed2toot
chown -R feed2toot:root /etc/feed2toot /var/lib/feed2toot

